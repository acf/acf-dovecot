local mymodule = {}

-- Load libraries
modelfunctions = require("modelfunctions")
fs = require("acf.fs")
format = require("acf.format")

-- Set variables
local configfile = "/etc/dovecot/dovecot.conf"
local processname = "dovecot"
local packagename = "dovecot"

local config

-- ################################################################################
-- LOCAL FUNCTIONS

-- ################################################################################
-- PUBLIC FUNCTIONS

function mymodule.get_startstop(self, clientdata)
        return modelfunctions.get_startstop(processname)
end

function mymodule.startstop_service(self, startstop, action)
        return modelfunctions.startstop_service(startstop, action)
end

function mymodule.getstatus()
	return modelfunctions.getstatus(processname, packagename, "Dovecot Status")
end

function mymodule.getstatusdetails()
	return cfe({ type="longtext", value="", label="Dovecot Status Details" })
end

function mymodule.get_filedetails()
	return modelfunctions.getfiledetails(configfile)
end

function mymodule.update_filedetails(self, filedetails)
	return modelfunctions.setfiledetails(self, filedetails, {configfile})
end

function mymodule.get_logfile(f)
	config = config or format.parse_ini_file(fs.read_file(configfile) or "", "")
	local files = {}
	if config and config.log_path then
		files[#files+1] = {filename = config.log_path}
	end
	if config and config.info_log_path then
		files[#files+1] = {filename = config.info_log_path}
	end
	if config and config.debug_log_path then
		files[#files+1] = {filename = config.debug_log_path}
	end
	if 0 == #files then
		if config and config.syslog_facility then
			files[#files+1] = {facility = config.syslog_facility, grep = "dovecot"}
		else
			files[#files+1] = {facility = "mail", grep = "dovecot"}
		end
	end
	return cfe({ type="structure", value=files, label="DoveCot Log Files" })
end

return mymodule
